var net = require('net');

postmessage();

function postmessage(req, res) {
    var dest = 'localhost';
    var port = '16665';
    var jsonstring = `MSH|^~\&|HIS|RIS|RAD|MDHC|20220610134546||ORU^R01|ABC-0000001|P|2.4|100
PID|||999999||สมใจ^แสนดี^^^นาง||19730512|F|Somjai^Sandee^^MRS.|||||||||||||||||||||
PV1|||^||||||||||||||||||||||||||||||||||||||||||||||||||
OBR|1||ABC-0000001|03GX01^Chest PA|||20220610134533|Normal||||||||||||||||||||||||||0114697|||||||||||
OBX|1||SR Text||Radiology Report History Cough Findings PA evaluation of the chest demonstrates the lungs to be expanded and clear. Conclusions Normal PA chest x-ray.||||||F||||||CTI|study1|^1|^10_EP1
OBX|2||||https://server_address:port/ViewReport?User=<user>&Password=<password>&ACCNO=ABC-0000001&PTNID=999999||||||||||||`;


    var VT = String.fromCharCode(0x0b);
    var FS = String.fromCharCode(0x1c);
    var CR = String.fromCharCode(0x0d);

    var sock = new net.Socket();
    
    sock.setTimeout(2500);

    sock.on('connect', function() {
        console.log(dest+':'+port+' is up.');
    }).on('error', function(e) {
        console.log(e);
        sock.destroy();
    }).on('timeout', function(e) {
        console.log(e);
        sock.destroy();
    }).connect(port, dest, function (d) {
        sock.write(VT + jsonstring + FS + CR);
    });

    sock.on('data', function () {
        sock.end();
    });
}